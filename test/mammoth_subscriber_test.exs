defmodule MammothSubscriberTest do
  use ExUnit.Case

  alias Mammoth.Subscriber

  require Logger

  # doctest Mammoth.Subscriber

  test "keeps track of subscriptions" do
    {:ok, pid} = Subscriber.start_link()
    Subscriber.subscribe(pid, "foo.bar", :auto)
    Subscriber.subscribe(pid, "foo2.bar", :auto)

    assert Subscriber.has_subscription?(pid, "foo.bar")
    assert Subscriber.has_subscription?(pid, "foo2.bar")
    assert !Subscriber.has_subscription?(pid, "foo3.bar")
  end

  test "do not allow double subscriptions" do
    {:ok, pid} = Subscriber.start_link()
    Subscriber.subscribe(pid, "foo.bar", :auto)
    {mode, _msg} = Subscriber.subscribe(pid, "foo.bar", :auto)

    assert mode == :error
    assert Subscriber.get_subscription(pid, "foo.bar").id == 1
  end

  test "delete subscriptions" do
    {:ok, pid} = Subscriber.start_link()
    Subscriber.subscribe(pid, "foo.bar", :auto)

    assert Subscriber.has_subscription?(pid, "foo.bar")

    Subscriber.unsubscribe(pid, "foo.bar")

    assert !Subscriber.has_subscription?(pid, "foo.bar")
  end
end
