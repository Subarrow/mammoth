defmodule Mammoth.Socket do
  alias Mammoth.Message

  require Logger

  def connect(host, port, send_timeout, recbuf) do
    {:ok, conn} = :gen_tcp.connect(host, port, [{:active, false}])

    :inet.setopts(conn, [
      {:buffer, recbuf || 0x400_000},
      {:recbuf, recbuf || 0x400_000},
      {:sndbuf, 0x400_000},
      {:send_timeout, send_timeout},
      {:send_timeout_close, true}
    ])

    {:ok, conn}
  end

  def send(conn, message = %Mammoth.Message{}) do
    __MODULE__.send(conn, Message.format(message))
  end

  def send(conn, message) when is_binary(message) do
    case :gen_tcp.send(conn, message) do
      :ok ->
        Logger.debug(inspect(message))

      {:error, reason} ->
        Logger.error(inspect(message))
        raise "Error sending: #{reason}"
    end
  end

  def receive(conn, length \\ 0) do
    response = :gen_tcp.recv(conn, length)

    case response do
      {:ok, response} -> Logger.debug(inspect(response, binaries: :as_strings))
      _ -> Logger.debug("Got response that was not :ok")
    end

    response
  end
end
