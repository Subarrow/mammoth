defmodule Mammoth.Receiver do
  use GenServer

  require Logger
  alias Mammoth.Message

  # Client

  def init(args) do
    args = Map.merge(args, %{partial_message: nil, last_heartbeat: 0})
    {:ok, args}
  end

  def terminate(_reason, _state = %{socket: socket}) when not is_nil(socket) do
    Logger.debug("Receiver is being terminated, closing socket")
    :gen_tcp.close(socket)
  end

  def start_link(state \\ %{}) do
    GenServer.start_link(__MODULE__, state)
  end

  def listen(pid) do
    GenServer.cast(pid, :listen)
  end

  def stop(server) do
    GenServer.stop(server)
  end

  defp heartbeat_with_rl(last_heartbeat, consumer) do
    time_now = :os.system_time(:millisecond)

    cond do
      time_now - last_heartbeat < 200 ->
        last_heartbeat

      true ->
        Mammoth.receive(consumer, :heartbeat)
        time_now
    end
  end

  # Server

  def handle_cast(:listen, state = %{socket: socket, consumer: consumer}) do
    case :gen_tcp.recv(socket, 0) do
      {:ok, response} ->
        Logger.debug(inspect(IO.iodata_to_binary(response), binaries: :as_strings))
        handle_response(state, response)

      {:error, :closed} ->
        GenServer.cast(consumer, :disconnected)
        {:noreply, state}
    end
  end

  @spec handle_response(map(), charlist()) :: {:noreply, map(), {:continue, :listen}}
  def handle_response(state, response)

  def handle_response(state, "") do
    {
      :noreply,
      state,
      {:continue, :listen}
    }
  end

  def handle_response(state, response) do
    state
    |> get_message(response)
    |> Message.parse()
    |> handle_message(state, response)
  end

  @spec handle_message({atom(), any()}, map(), charlist()) ::
          {:noreply, map(), {:continue, :listen}}
  defp handle_message(
         {:incomplete, _message},
         state = %{consumer: consumer, last_heartbeat: last_heartbeat},
         response
       ) do
    Logger.debug("incomplete message, continue listening...")

    last_heartbeat = heartbeat_with_rl(last_heartbeat, consumer)

    {
      :noreply,
      %{state | partial_message: get_message(state, response), last_heartbeat: last_heartbeat},
      {:continue, :listen}
    }
  end

  defp handle_message(
         {:ok, message, more},
         state = %{consumer: consumer, last_heartbeat: last_heartbeat},
         _response
       ) do
    Logger.debug(inspect(message))

    last_heartbeat =
      if message == :heartbeat,
        do: heartbeat_with_rl(last_heartbeat, consumer),
        else: last_heartbeat

    if message != :heartbeat do
      Mammoth.receive(consumer, message)
    end

    if byte_size(more) > 0 do
      handle_response(
        %{state | partial_message: [], last_heartbeat: last_heartbeat},
        :binary.bin_to_list(more)
      )
    else
      {:noreply,
       %{state | partial_message: :binary.bin_to_list(more), last_heartbeat: last_heartbeat},
       {:continue, :listen}}
    end
  end

  defp handle_message(_, state = %{consumer: consumer}, response) do
    Logger.warn("Unable to parse response: #{inspect(IO.iodata_to_binary(response),
    limit: :infinity,
    printable_limit: :infinity,
    pretty: true)}")

    GenServer.cast(consumer, {:fatal, :parse_error})

    {:noreply, state, {:continue, :listen}}
  end

  def handle_continue(:listen, state) do
    GenServer.cast(self(), :listen)
    {:noreply, state}
  end

  @spec get_message(map(), charlist()) :: charlist()
  defp get_message(state, message) do
    case Map.get(state, :partial_message) do
      nil ->
        message

      partial_message ->
        partial_message ++ message
    end
  end
end
