defmodule Mammoth.DefaultCallbackHandler do
  @moduledoc """
  Default callback handler, useful for basic testing, and as a complete example for
  how to implement your own callback handler

  ## Example
      {:ok, callback_pid} = Mammoth.DefaultCallbackHandler.start_link
      {:ok, mammoth_pid} = Mammoth.start_link(callback_pid)
  """
  use GenServer
  alias Mammoth.Message
  require Logger

  def start_link do
    GenServer.start_link(__MODULE__, nil)
  end

  def init(_) do
    {:ok, %{}}
  end

  @doc """
  Set Mammoth PID. This is to make examples simpler to follow, my recommendation is instead to
  start and link Mammoth from within your own callback genserver implementation
  """
  def set_mammoth_pid(callback_pid, mammoth_pid) do
    GenServer.call(callback_pid, {:set_mammoth_pid, mammoth_pid})
  end

  def handle_call({:set_mammoth_pid, pid}, _from, state) do
    {:reply, :ok, state |> Map.put(:mammoth_pid, pid)}
  end

  def handle_info({:mammoth, :receive_frame, message = %Message{command: :error}}, state) do
    Logger.error("Mammoth error: #{inspect(message, binaries: :as_strings)}")
    {:noreply, state}
  end

  def handle_info(
        {:mammoth, :receive_frame, message = %Message{command: :message}},
        state = %{mammoth_pid: mammoth_pid}
      ) do
    Logger.debug("Mammoth message received: #{inspect(message, binaries: :as_strings)}")
    Mammoth.send_ack_frame(mammoth_pid, message)
    {:noreply, state}
  end

  def handle_info({:mammoth, :receive_frame, message = %Message{command: :connected}}, state) do
    Logger.info("Mammoth connection success: #{inspect(message, binaries: :as_strings)}")
    {:noreply, state}
  end

  def handle_info({:mammoth, :receive_frame, message}, state) do
    Logger.debug("Some other sort of message: #{inspect(message, binaries: :as_strings)}")
    {:noreply, state}
  end

  def handle_info({:mammoth, :disconnected, :local}, state) do
    Logger.info("Mammoth confirmation received for disconnection")
    {:noreply, state}
  end

  def handle_info({:mammoth, :disconnected, :remote}, state) do
    Logger.error("Mammoth connection closed by remote host")
    {:noreply, state}
  end

  def handle_info({:mammoth, :disconnected, :parse_error}, state) do
    Logger.error("Mammoth connection closed by local end because of a parsing error")
    {:noreply, state}
  end

  def handle_info({:mammoth, :disconnected, :timeout}, state) do
    Logger.error("Mammoth connection closed by local end because of a timeout")
    {:noreply, state}
  end
end
