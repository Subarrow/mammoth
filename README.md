# Mammoth

An Elixir STOMP client based on eteubert's Elephant, intended to improve
properties useful for binary queues such as National Rail's Darwin, and
features useful for submitting to queues.

You can find the original code here:
https://github.com/eteubert/elephant
Original documentation:
https://hexdocs.pm/elephant/Elephant.html

## Avatar
CC0 licence, by Frost (https://frost.brightfur.net/)

## Status
Not recommended for production use.

## Installation
First, add Mammoth to your `mix.exs` dependencies:

```elixir
def deps do
    [{:mammoth, "~>0.5.3"}]
end
```

Then, update your dependencies:

```sh-session
$ mix deps.get
```

## Usage
See https://hexdocs.pm/mammoth/Mammoth.html

## Licence
MIT
