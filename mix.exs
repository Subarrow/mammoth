defmodule Mammoth.MixProject do
  use Mix.Project

  def project do
    [
      app: :mammoth,
      description:
        "A STOMP client to listen to, for example, AMQ. A fork of ericteubert's Elephant package.",
      version: "0.5.3",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package(),
      source_url: "https://codeberg.org/Subarrow/mammoth",
      docs: [
        main: "Mammoth"
      ]
    ]
  end

  defp package() do
    [
      maintainers: ["Subarrow"],
      licenses: ["MIT"],
      links: %{
        "Source code" => "https://codeberg.org/Subarrow/mammoth",
        "Original source code" => "https://github.com/eteubert/elephant"
      }
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Mammoth.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_guard, "~> 1.3.0", only: :dev},
      {:ex_doc, "~> 0.28.4", only: :dev, runtime: false},
      {:credo, "~> 1.6", only: [:dev, :test], runtime: false}
    ]
  end
end
